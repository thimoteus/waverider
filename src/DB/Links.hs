{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
module DB.Links where

import Config (dbName)
import Lib (now, handleMaybe)
import Types.Users (User(..))
import Types.Links (Link(..))

import Control.Monad (guard, when)
import Control.Lens ((^.))
import Data.ByteString.Lazy (ByteString, fromStrict)
import qualified Data.ByteString.Lazy.Char8 as BS
import Data.ByteString.Lazy.Search (breakAfter)
import Data.List (groupBy)
import Database.HDBC (run, commit, quickQuery', toSql, fromSql, executeMany, prepare)
import Database.HDBC.Types (IConnection)
import Network.Wreq (FormParam(..), responseBody, get, post, responseHeader)

submitIO :: IConnection conn => conn -> Link -> IO ()
submitIO conn Link {ownerID, url, tags, archive} = do
  date <- now
  arch <- if null archive
    then getArchive url
    else pure $ BS.pack archive
  run conn insertLink $ linkVals date arch
  lastRowId <- quickQuery'
    conn
    "select linkID from links where linkID = last_insert_rowid();"
    []
  case lastRowId of
    [[x]] -> let lrid = fromSql x in do
      stmnt <- prepare conn insertTag
      executeMany stmnt $ tagVals (lrid :: Int)
      commit conn
    _ -> fail "Something went wrong when submitting a link"
    where
    insertLink
      = "insert into links (ownerID, url, date, archive) values (?, ?, ?, ?);"
    linkVals date arch
      = [ toSql ownerID
        , toSql url
        , toSql (date :: Integer)
        , toSql (arch :: ByteString)
        ]
    insertTag = "insert into tags (linkID, tag) values (?, ?);"
    tagVals linkID = map (\ x -> [toSql $ show linkID, toSql x]) tags

getLinkIO :: IConnection conn => conn -> User -> IO [Link]
getLinkIO conn u = do
  groupedLinks <- groupBy sameId <$> quickQuery' conn getLinks linkVals
  pure $ map toLinks groupedLinks
    where
    getLinks = "select links.linkID, links.date, links.url, links.archive, tags.tag from links inner join tags on links.linkID = tags.linkID where links.ownerID = ? order by links.linkID;"
    linkVals = [toSql $ userID u]
    sameId (x1 : _) (y1 : _) = x1 == y1
    sameId _ _ = False
    toLinks = foldr foldLink dummyLink
    dummyLink = Link { ownerID = 0, url = "", archive = "", date = 0, tags = [], linkID = 0 }
    foldLink [linksid, linksdate, linksurl, linksarch, tagstag] user =
      Link { ownerID = 0
           , url = fromSql linksurl
           , date = fromSql linksdate
           , tags = fromSql tagstag : tags user
           , linkID = 0
           , archive = fromSql linksarch
           }

getArchive :: String -> IO ByteString
getArchive url
  = handleMaybe archiveDown getSubmitIdIO
  >>= handleMaybe unknownErr . getArchiveUrl url
  where
    archiveDown = "Could not access archive.is, try again later"
    unknownErr = "Something went wrong while submitting to archive.is"

getSubmitIdIO :: IO (Maybe ByteString)
getSubmitIdIO = do
  r <- get "http://archive.is/"
  pure $ getSubmitId $ r ^. responseBody

getSubmitId :: ByteString -> Maybe ByteString
getSubmitId body = do
  let (_, cdr) = breakAfter "name=\"submitid\" value=" body
  (c, bs) <- BS.uncons cdr
  guard (c == '"')
  pure $ BS.takeWhile (/= '"') bs

getArchiveUrl :: String -> ByteString -> IO (Maybe ByteString)
getArchiveUrl url submitid = do
  r <- post "http://archive.is/submit/"
    ["submitid" := submitid
    , "url" := url
    , "anyway" := ("1" :: String)
    ]
  pure $ extractUrl $ fromStrict $ r ^. responseHeader "Refresh"

extractUrl :: ByteString -> Maybe ByteString
extractUrl resp = do
  let (car, cdr) = BS.splitAt 6 resp
  guard $ car == "0;url="
  pure cdr
