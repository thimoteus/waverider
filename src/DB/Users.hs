{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
module DB.Users where

import Config (dbName)
import DB.General (genericQuery)
import Types.Users (User(..))
import Lib (secret, nowDiffTime, addDay)

import Crypto.PasswordStore (makePassword, verifyPassword)
import Database.HDBC (SqlValue, run, commit, quickQuery', toSql, fromSql)
import Database.HDBC.Types (IConnection)
import Data.Aeson (toJSON)
import Data.ByteString (ByteString)
import Data.Default (def)
import Data.Map (singleton)
import Data.String (fromString)
import Data.Text (pack)
import Data.Text.Lazy (Text, fromStrict)
import Data.Time.Clock (NominalDiffTime, secondsToDiffTime)
import Web.JWT (Algorithm(..), JWTClaimsSet(..), binarySecret, stringOrURI, encodeSigned, numericDate, unregisteredClaims)

register :: IConnection conn => conn -> User -> IO ()
register conn u@User {email} = do
  exists <- emailCheck conn email
  if exists
    then fail "Email already in use"
    else registerUser conn u

-- used when registering a new user, to see if the provided email has been used already.
emailCheck :: IConnection conn => conn -> String -> IO Bool
emailCheck conn e = genericQuery conn stmnt (not . null) vals
  where
  stmnt = "select * from users where email = ?;"
  vals = [toSql e]

registerUser :: IConnection conn => conn -> User -> IO ()
registerUser conn User {email, username, userpass} = do
  newPass <- makePassword (fromString userpass) 16
  run conn stmnt [toSql email, toSql username, toSql newPass]
  commit conn
  where
    stmnt = "insert into users(email, name, pass) values (?, ?, ?);"

-- Check the password against the database for a given email (username in input
-- is not important). If it matches, give back a new User with the proper
-- username and ID in the database.
login :: IConnection conn => conn -> User -> IO (Maybe Text)
login conn u@User {email, userpass} = do
  s <- secret
  t <- nowDiffTime
  genericQuery conn stmnt (f s t) vals
    where
    stmnt = "select userID, name, pass from users where email = ?;"
    vals = [toSql email]
    f s t [[i, n, p]] = if verifyPassword (fromString userpass) (fromSql p)
      then Just $ tokenize s t (fromSql i) email
      else Nothing
    f _ _ _ = Nothing

tokenize :: ByteString -> NominalDiffTime -> Int -> String -> Text
tokenize sec time ident mail =
  fromStrict $ encodeSigned HS256 (binarySecret sec) jwtcs
    where
    jwtcs =
      def { iss = stringOrURI "waverider"
          , sub = stringOrURI $ pack mail
          , Web.JWT.exp = numericDate $ addDay time
          , unregisteredClaims = singleton "id" $ toJSON ident
          }
