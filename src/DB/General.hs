module DB.General where

import Config (dbName)

import Database.HDBC (SqlValue, run, commit, disconnect, quickQuery', toSql, fromSql)
import Database.HDBC.Types (IConnection)

-- Arguments are: a SQL statement, a function on the results of the query,
-- and a list of values to bind
genericQuery :: IConnection conn => conn -> String -> ([[SqlValue]] -> a) -> [SqlValue] -> IO a
genericQuery conn stmnt f vals = do
  res <- quickQuery' conn stmnt vals
  commit conn
  pure $ f res
