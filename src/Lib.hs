module Lib where

import Data.Maybe (maybe)
import Data.Time.Calendar (fromGregorian)
import Data.Time.Clock (UTCTime(..), NominalDiffTime, diffUTCTime)
import Data.Time.Clock.POSIX (getCurrentTime)
import Data.ByteString (ByteString)
import qualified Data.ByteString as IO (readFile)
import Web.Scotty (ActionM, liftAndCatchIO)

monoidify :: (Monad m, Foldable t) => t (m a) -> m ()
monoidify = foldr (>>) (pure ())

say :: Show a => a -> ActionM ()
say = liftAndCatchIO . print

epoch :: UTCTime
epoch = UTCTime (fromGregorian 1970 1 1) 0

timeAsInteger :: UTCTime -> Integer
timeAsInteger = floor . flip diffUTCTime epoch

now :: IO Integer
now = timeAsInteger <$> getCurrentTime

nowDiffTime :: IO NominalDiffTime
nowDiffTime = flip diffUTCTime epoch <$> getCurrentTime

secret :: IO ByteString
secret = IO.readFile "secret"

addDay :: NominalDiffTime -> NominalDiffTime
addDay t = t + diffUTCTime (UTCTime (fromGregorian 1970 1 2) 0) epoch

handleMaybe :: String -> IO (Maybe a) -> IO a
handleMaybe msg ioma = do
  ma <- ioma
  maybe (fail msg) pure ma
