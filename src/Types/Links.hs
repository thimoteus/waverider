{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
module Types.Links where

import Data.Maybe (fromMaybe)
import Data.Aeson (ToJSON, FromJSON, (.:), (.:?), (.=), Value(..), object, toJSON, parseJSON)

data Link = Link { ownerID :: Int
                 , url :: String
                 , date :: Integer
                 , tags :: [String]
                 , linkID :: Int
                 , archive :: String
                 } deriving (Eq, Show)

instance ToJSON Link where
  toJSON link
    = object
    [ "url" .= url link
    , "date" .= date link
    , "tags" .= tags link
    , "archive" .= archive link
    ]

instance FromJSON Link where
  parseJSON (Object u) = do
    url <- u .: "url"
    d <- u .:? "date"
    tags <- u .: "tags"
    a <- u .:? "archive"
    let date = fromMaybe 0 d
        archive = fromMaybe "" a
    pure Link {date, url, tags, archive, linkID = 0, ownerID = 0}
