{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module Types.JSON where

import GHC.Generics (Generic)

import Data.Aeson (ToJSON, FromJSON)
import Data.Text.Lazy (Text)

import Web.Scotty (ActionM, json)

data JsonErr
  = JsonErr
  { err :: Maybe Text
  } deriving (Generic)

instance FromJSON JsonErr
instance ToJSON JsonErr

jsonErr :: Text -> ActionM ()
jsonErr = json . JsonErr . Just

data JsonMsg
  = JsonMsg
  { msg :: Text
  } deriving (Generic)

instance FromJSON JsonMsg
instance ToJSON JsonMsg

jsonMsg :: Text -> ActionM ()
jsonMsg = json . JsonMsg
