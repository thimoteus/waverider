{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
module Types.Users where

import Control.Applicative (empty)
import Data.Aeson (ToJSON, FromJSON, (.:), (.:?), (.=), Value(..), object, toJSON, parseJSON)
import Data.Maybe (fromMaybe)

data User =
  User { username :: String
       , userpass :: String
       , email :: String
       , userID :: Int
       } deriving (Show, Eq)

instance FromJSON User where
  parseJSON (Object u) = do
    un <- u .:? "username"
    userpass <- u .: "password"
    email <- u .: "email"
    let username = fromMaybe "" un
    pure User {userpass, email, username, userID = 0}
  parseJSON _ = empty

newUser :: String -> String -> String -> User
newUser x y z = User {username = x, userpass = y, email = z, userID = 0}

instance ToJSON User where
  toJSON User {username, userpass, email}
    = object ["username" .= username, "password" .= userpass, "email" .= email]
