module Config where

dbName :: String
dbName = "users.db"

port :: Num t => t
port = 3001
