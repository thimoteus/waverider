{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
module Actions.General where

import Types.JSON (jsonErr)
import Types.Users (User(..))
import Lib (secret, nowDiffTime)

import Control.Monad (unless)
import Data.ByteString (ByteString)
import Data.Aeson (decode, encode)
import qualified Data.Map as Map (lookup)
import Data.Text (unpack)
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text
import Data.Time.Clock (NominalDiffTime)
import Database.HDBC.Types (IConnection)
import Network.HTTP.Types (badRequest400, status401)
import Web.JWT (JWTClaimsSet, decodeAndVerifySignature, claims, sub, exp, binarySecret, stringOrURIToText, secondsSinceEpoch, unregisteredClaims)
import Web.Scotty (ActionM, status, header, liftAndCatchIO, finish)

badRequest :: Text -> ActionM ()
badRequest err = do
  jsonErr err
  status badRequest400
  finish

unauthorized :: ActionM ()
unauthorized = do
  jsonErr "unauthorized"
  status status401

-- Run a continuation that assumes a logged-in user. If the credentials check out
-- the continuation will fire. If not, will set an unauthorized status.
withUser :: IConnection conn => conn -> (User -> ActionM a) -> ActionM a
withUser conn k = do
  authed <- header "Authorization"
  s <- liftAndCatchIO secret
  n <- liftAndCatchIO nowDiffTime
  case authed >>= decodeAuth s n of
    Just u -> k u
    _ -> unauthorized >> finish

decodeAuth :: ByteString -> NominalDiffTime -> Text -> Maybe User
decodeAuth sec currentTime txt =
  case Text.splitAt 7 txt of
    ("Bearer ", gobbdledygook) -> verify currentTime sec gobbdledygook
    _ -> Nothing

verify :: NominalDiffTime -> ByteString -> Text -> Maybe User
verify currentTime sec txt = do
  jwt <- decodeAndVerifySignature (binarySecret sec) $ Text.toStrict txt
  let cls = claims jwt
  expiration <- Web.JWT.exp cls
  unless (secondsSinceEpoch expiration >= currentTime) Nothing
  mail <- sub cls
  ident <- Map.lookup "id" $ unregisteredClaims cls
  userID <- decode $ encode ident
  pure User {email = unpack $ stringOrURIToText mail, username = "", userpass = "", userID}

-- decodeBasicAuth :: Text -> Maybe User
-- decodeBasicAuth txt = do
--   sixtyfour <- get64 txt
--   plain <- unscramble sixtyfour
--   (email, userpass) <- getEmailAndPass plain
--   pure User {email, username = "", userpass, userID = 0}
--     where
--     get64 t = case Text.splitAt 6 t of
--       ("Basic ", x) -> pure x
--       _ -> Nothing
--     unscramble = either (const Nothing) (pure . Text.fromStrict . decodeUtf8) . un64
--     un64 = decode . encodeUtf8 . Text.toStrict
--     getEmailAndPass code = case Text.splitOn ":" code of
--       [m, p] -> pure (Text.unpack m, Text.unpack p)
--       _ -> Nothing
