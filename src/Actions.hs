{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NamedFieldPuns #-}
module Actions where

import Actions.General (badRequest, withUser)
import qualified DB.Users as Users
import qualified DB.Links as Links
import Types.Users (User(..))
import Types.JSON (jsonErr, jsonMsg)
import Types.Links (Link(..))

import qualified Data.Text.Lazy.IO as IO
import Web.Scotty (ActionM, json, file, rescue, liftAndCatchIO, jsonData)
import Database.HDBC.Types (IConnection)

home :: ActionM ()
home = file "index.html"

register :: IConnection conn => conn -> ActionM ()
register conn = flip rescue badRequest $ do
  newUser <- jsonData
  liftAndCatchIO $ Users.register conn newUser
  jsonMsg "Registration successful. You may now log in with your credentials."

login :: IConnection conn => conn -> ActionM ()
login conn = flip rescue badRequest $ do
  user <- jsonData
  token <- liftAndCatchIO $ Users.login conn user
  maybe (jsonErr "Login credentials incorrect") jsonMsg token

submitLink :: IConnection conn => conn -> ActionM () -- NOTE: will print out internal errors to json
submitLink conn = flip rescue jsonErr $ withUser conn $ \ u -> do
  link1 <- jsonData
  liftAndCatchIO $
    Links.submitIO
      conn
      Link { url = url link1
           , ownerID = userID u
           , date = 0
           , tags = tags link1
           , linkID = 0
           , archive = archive link1
           }
  jsonMsg "Link submission successful"

getLinks :: IConnection conn => conn -> ActionM ()
getLinks conn = withUser conn getLinks'
  where
  getLinks' user = liftAndCatchIO (Links.getLinkIO conn user) >>= json
