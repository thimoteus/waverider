{-# LANGUAGE OverloadedStrings #-}
module Routes where

import Actions (register, home, login, submitLink, getLinks)
import Web.Scotty (ScottyM, get, post, middleware)
import Network.Wai.Middleware.Static (staticPolicy, addBase, noDots, (>->))
import Database.HDBC.Types (IConnection)

routes :: IConnection conn => conn -> ScottyM ()
routes conn = do
  middleware $ staticPolicy $ noDots >-> addBase "assets"
  get "/" home
  get "/links.json" $ getLinks conn
  post "/register" $ register conn
  post "/login" $ login conn
  post "/submit" $ submitLink conn
