module Main where

import Routes (routes)
import Config (dbName, port)

import Data.Monoid ((<>), mconcat)

import Web.Scotty (scotty)

import Database.HDBC (run, commit)
import Database.HDBC.Sqlite3 (connectSqlite3)

main :: IO ()
main = do
  putStrLn "Initializing database"
  conn <- connectSqlite3 dbName
  traverse (\ x -> run conn x []) [createUsers, createLinks, createTags]
  commit conn
  scotty port $ routes conn
  where
    createUsers =
      mconcat [ "create table if not exists users ("
              , "userID integer primary key autoincrement,"
              , "email text not null unique,"
              , "name text not null,"
              , "pass text not null);" ]
    createLinks =
      mconcat [ "create table if not exists links ("
              , "linkID integer primary key autoincrement,"
              , "ownerID integer not null,"
              , "url text not null,"
              , "date integer not null,"
              , "archive text not null,"
              , "foreign key (ownerID) references users(userID));" ]
    createTags =
      mconcat [ "create table if not exists tags("
              , "tagID integer primary key autoincrement,"
              , "linkID integer not null,"
              , "tag text not null,"
              , "foreign key (linkID) references links(linkID));" ]
